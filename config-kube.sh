#!/bin/bash

###############################################################################
# get kubernetes cluster credentials from gcloud and verify
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh
source env.sh

# fail on error or undeclared vars
trap_errors

gcloud container clusters get-credentials ${GCP_CLUSTER_NAME}

if ! kubectl version &> /dev/null; then
  kubectl version || true
  echo
  echo ERROR: cannot connect to kubernetes cluster!
  echo
fi
