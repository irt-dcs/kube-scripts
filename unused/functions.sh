#!/bin/bash
set -e

## Replace placeholders with values
# $1: file with placeholders to replace
# $x: placeholders to replace
# e.g.
# update_template app.yml env.vars \
#    FOO \
#    BAR \
#    BUILD_NUMBER
#
function update_template() {
    local FILE=${1?missing template}
    shift
    local ENV_FILE=${1?missing env.vars}
    shift
    [[ ! -f ${FILE} ]] && return 1
    [[ ! -f ${ENV_FILE} ]] && return 1

    source ${ENV_FILE}
    local VARIABLES=($@)
    local tmp_file=$(mktemp)
    cp -a "${FILE}" ${tmp_file}

    local variable
    for variable in ${VARIABLES[@]}; do
        # Keep the compatibilty: {{VAR}} => ${VAR}
        sed -ri "s/[{]{2}$variable[}]{2}/\${$variable}/g" ${tmp_file}
        # For MacOS
        #sed -Ei '' "s/[{]{2}$variable[}]{2}/\${$variable}/g" ${tmp_file}
    done

    # Replace placeholders
    (
        export ${VARIABLES[@]}
        local IFS=":"; envsubst "${VARIABLES[*]/#/$}" < ${tmp_file} > ${FILE}
    )
    rm -f ${tmp_file}
}
