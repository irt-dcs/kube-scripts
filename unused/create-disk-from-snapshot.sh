#!/bin/bash

# Usage: $0 <disk name> <zone> <size> <type> <snapshot>

DISK_NAME=${1:-$DISK_NAME}
ZONE=${2:-$ZONE}
DISK_SIZE=${3:-$DISK_SIZE}
DISK_TYPE=${4:-gd2}
SNAPSHOT=${5:-$SNAPSHOT}

gcloud compute disks create ${DISK_NAME} \
    --zone ${ZONE} \
    --size ${DISK_SIZE} \
    --type ${DISK_TYPE} \
    --source-snapshot ${SNAPSHOT}
