#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <svc-name>"
fi

kubectl get svc $1 -o json | jq -r '.spec.ports[].port'
