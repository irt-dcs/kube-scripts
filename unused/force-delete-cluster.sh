#!/bin/bash

GCP_CLUSTER_NAME=${1:-$GCP_CLUSTER_NAME}
GCP_ZONE=${2:-$GCP_ZONE}

gcloud container clusters delete "${GCP_CLUSTER_NAME}" --zone "${GCP_ZONE}" --quiet
