#!/bin/bash

# useage $0 <ip-name>

ADDR_NAME=$1

gcloud compute addresses create ${ADDR_NAME} --global
