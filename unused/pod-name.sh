#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <app label> [<pod-zero-based-index>]"
fi

index=${2:-0}
kubectl get pod -l app=$1 -o json | jq -r --arg i $index '.items[$i | tonumber].metadata.name'
