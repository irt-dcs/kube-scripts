#!/bin/bash

# force-create-delete-ip.sh name

ADDR_NAME=$1

gcloud compute addresses delete ${ADDR_NAME} --global --quiet
