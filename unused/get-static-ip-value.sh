#!/bin/bash

# useage $0 <ip-name>

ADDR_NAME=$1

IP=$(gcloud compute addresses describe $ADDR_NAME --global --format 'value(address)' 2>/dev/null)

if [ -z ${IP} ]
then
    gcloud compute addresses create ${ADDR_NAME} --global
    IP=$(gcloud compute addresses describe $ADDR_NAME --global --format 'value(address)')
fi
echo $IP
