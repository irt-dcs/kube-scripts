#!/bin/bash

DISK_NAME=${1:-$DISK_NAME}
DISK_ZONE=${2:-$DISK_ZONE}
TMS=$(date +"%F-%H-%M-%S-%3N%Z")

gcloud compute disks snapshot ${DISK_NAME} \
    --zone ${DISK_ZONE} \
    --snapshot-names ${DISK_NAME}-${TMS}
