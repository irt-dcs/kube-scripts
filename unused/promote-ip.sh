#!/bin/bash

ADDR_NAME=$1
ADDR_IP=$2

gcloud compute addresses create ${ADDR_NAME} --addresses ${ADDR_IP}
