#!/bin/bash

# useage $0 <ip-name>

ADDR_NAME=$1

gcloud compute addresses delete ${ADDR_NAME} --global
