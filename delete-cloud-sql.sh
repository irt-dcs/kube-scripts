#!/bin/bash

###############################################################################
# Destroys a cloud sql instance
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

# include functions
source $THIS_DIR/functions.sh

instance_prefix=$1
failover_instance_prefix=$2

instance_name=$($THIS_DIR/cloud-sql-prefix-to-name.sh $instance_prefix)
failover_instance_name=$($THIS_DIR/cloud-sql-prefix-to-name.sh $failover_instance_prefix)

# delete failover replica first if exists
echo
echo "DELETING cloud SQL failover instance '$failover_instance_name'"
gcloud sql instances delete $failover_instance_name

echo
echo "DELETING cloud SQL master instance '$instance_name'"
gcloud sql instances delete $instance_name
