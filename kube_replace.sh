#!/bin/bash

###############################################################################
# replace resource definitions using list of input templates
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh
source env.sh

# fail on error or undeclared vars
trap_errors

template_files=$@

echo replacing kubernetes resources with templates:
for template in $template_files; do
  echo "   $template"
  cat $template | envsubst | kubectl replace --force -f -
done
