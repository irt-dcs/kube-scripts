#!/bin/bash

###############################################################################
# add a repo to drone
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PATH=$PATH:$THIS_DIR

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

if ! drone-cli.sh repo info ${DRONE_REPO} > /dev/null 2>&1; then
  drone-cli.sh repo add ${DRONE_REPO}
fi
