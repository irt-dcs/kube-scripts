#!/bin/bash

###############################################################################
# Setup a NAT server and routing all traffic to DEST_RANGES
# through the nat-server
# Test with
#    $ traceroute library.stanford.edu
#
#    ____________________________
#   |          GCP               |
#   |      [ CLUSTER ]           |
#   |           |                |
#   |   [ GCP Route Rules (3) ]  | \
#   |           |                | |
#   |   [ NAT VM Instance (2) ]  | |-- this script
#   |         (link)             | |
#   |    [ GCP STATIC IP (1) ]   | /
#   ------------------------------
#               |
#    ____________________________
#   |        Stanford           |
#   |           |               |
#   | [   Stanford Firewall   ] |
#   | [  allows GCP STATIC IP ] |
#   |           |               |
#   |   [ Stanford Services ]   |
#   -----------------------------
#
###############################################################################

NETWORK=${NETWORK:-default}
NAT_MACHINE_TYPE=${NAT_MACHINE_TYPE:-g1-small}
IMAGE=${IMAGE:-debian-8}
IMAGE_PROJECT=${IMAGE_PROJECT:-debian-cloud}
GCP_NAT_PREFIX=${GCP_NAT_PREFIX:-stanford}
GCP_ZONE=${GCP_ZONE:-us-west1-a}
GCP_REGION=$(echo $GCP_ZONE | cut -d'-' -f 1-2)

# (1) CREATE A STATIC IP FOR NET-GATEWAY IF DOESN'T EXIST
if gcloud compute addresses describe ${GCP_NAT_PREFIX}-nat-gateway --region ${GCP_REGION} &> /dev/null
then
  echo static ip ${GCP_NAT_PREFIX}-nat-gateway already exsited.
else
  echo creating static ${GCP_NAT_PREFIX}-nat-gateway ...
  gcloud compute addresses create ${GCP_NAT_PREFIX}-nat-gateway --region ${GCP_REGION}
fi

# (2) CREATE THE NAT-GATEWAY INSTANCE IF DOESN'T EXIST
# create nat-gateway startup script
read -r -d '' STARTUP_SCRIPT <<'EOF'
#!/bin/bash
sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
EOF

if gcloud compute instances describe ${GCP_NAT_PREFIX}-nat-gateway &> /dev/null
then
  echo ${GCP_NAT_PREFIX}-nat-gateway already exsited.
else
  echo creating ${GCP_NAT_PREFIX}-nat-gateway ...
  gcloud compute instances create ${GCP_NAT_PREFIX}-nat-gateway \
    --network ${NETWORK} \
    --address ${GCP_NAT_PREFIX}-nat-gateway \
    --can-ip-forward \
    --zone ${GCP_ZONE} \
    --image-family ${IMAGE} \
    --image-project ${IMAGE_PROJECT} \
    --machine-type ${NAT_MACHINE_TYPE}\
    --metadata "startup-script=${STARTUP_SCRIPT}" \
    --tags nat-server
fi

# (3) CREATE ROUTES FOR NAT, IF DON'T EXIST
# See create-nat-routes.sh

# OUT PUT nat-gateway's IP
addr=$(gcloud compute addresses describe ${GCP_NAT_PREFIX}-nat-gateway --region ${GCP_REGION} | grep 'address:')

echo The nat-gateway static ip: $addr
