#!/bin/bash

###############################################################################
# Create a cluster of nodes to act as hosts for kubernetes container cluster
###############################################################################

# usage $0 <cluster name> <zone> <machine type> <number of nodes> <user> <pwd>

# get command-line input, set default parameters
GKE_CLUSTER_NAME=${1:-$GKE_CLUSTER_NAME}
GKE_CLUSTER_ZONE=${2:-$GKE_CLUSTER_ZONE}
GKE_CLUSTER_MACHINE_TYPE=${3:-$GKE_CLUSTER_MACHINE_TYPE}
GKE_CLUSTER_NUM_NODES=${4:-$GKE_CLUSTER_NUM_NODES}
GKE_CLUSTER_ADDITIONAL_ZONES=${5:-$GKE_CLUSTER_ADDITIONAL_ZONES}

GKE_CLUSTER_USER=${GKE_CLUSTER_USER:-admin}
DEFAULT_CLUSTER_PWD=$(openssl rand -base64 24)
GKE_CLUSTER_PWD=${GKE_CLUSTER_PWD:-$DEFAULT_CLUSTER_PWD}

# exit successfully if container cluster already exists
if gcloud container clusters describe "${GKE_CLUSTER_NAME}" > /dev/null 2>&1; then
  echo "Cluster ${GKE_CLUSTER_NAME} exist"
  exit
fi

# Prefer to use long form in future
# read -r -d '' SCOPES <<EOF
# https://www.googleapis.com/auth/compute,
# https://www.googleapis.com/auth/devstorage.read_only,
# https://www.googleapis.com/auth/logging.write,
# https://www.googleapis.com/auth/monitoring,
# https://www.googleapis.com/auth/cloud-platform,
# https://www.googleapis.com/auth/servicecontrol,
# https://www.googleapis.com/auth/service.management
# EOF

if ! [ -z ${GKE_CLUSTER_ADDITIONAL_ZONES} ]; then
  GKE_CLUSTER_ADDITIONAL_ZONES_PARAM="--additional-zones ${GKE_CLUSTER_ADDITIONAL_ZONES}"
fi

# create cluster
gcloud beta container clusters create "${GKE_CLUSTER_NAME}" \
  ${GKE_CLUSTER_ADDITIONAL_ZONES_PARAM} \
  --zone "${GKE_CLUSTER_ZONE}" \
  --machine-type "${GKE_CLUSTER_MACHINE_TYPE}" \
  --num-nodes ${GKE_CLUSTER_NUM_NODES} \
  --username "${GKE_CLUSTER_USER}" \
  --password "${GKE_CLUSTER_PWD}" \
  --enable-cloud-logging \
  --enable-cloud-monitoring \
  --scopes compute-rw,cloud-platform,storage-rw,service-control,service-management,monitoring,logging-write \
  --no-async \
  --enable-autorepair
