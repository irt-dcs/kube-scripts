# !/bin/bash
# CREATE ROUTES FOR NAT, IF DON'T EXIST

# NAT routes are defined in GCP_NAT_DEST_FILE
# must in format:
# <route name><space-or-tab><destination CIDR>

GCP_NAT_DEST_FILE=${1:-nat-destinations.conf}
NETWORK=${NETWORK:-default}
GCP_ZONE=${GCP_ZONE:-us-west1-a}
GCP_NAT_PREFIX=${GCP_NAT_PREFIX:-stanford}
PRIORITY=${PRIORITY:-800}

if [[ ! -f ${GCP_NAT_DEST_FILE} ]]
then
  echo "File ${GCP_NAT_DEST_FILE}: not found"
  exit
fi

while read -r line; do
  if ! [[ (-z "$line" )  || ( $line == \#* ) ]]
  then
    route_name=`echo $line | awk '{print $1}'`; \
    dest=`echo $line | awk '{print $2}'`; \
    if gcloud compute routes describe ${route_name} &> /dev/null
    then
      echo ${route_name} already exsited.
    else
      echo Creating route ${route_name} ...
      gcloud compute routes create ${route_name} \
        --network ${NETWORK} \
        --destination-range "${dest}" \
        --next-hop-instance ${GCP_NAT_PREFIX}-nat-gateway \
        --next-hop-instance-zone ${GCP_ZONE} \
        --priority ${PRIORITY} \
        --description "route ${dest} traffic through ${GCP_NAT_PREFIX}-nat-gateway"
    fi
  fi
done < ${GCP_NAT_DEST_FILE}
