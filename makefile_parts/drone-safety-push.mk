# makefile targets which prevent concurrent builds
# push makes sure
# push

.PHONY: push
push: ## safely push git changes to deploy in current environment context
	$(MAKE) push-${API_ENV}

.PHONY: push-stage
push-stage:
	@if [ -z "$$(drone-cli.sh build list ${DRONE_REPO} --status running --branch master)" ]; then \
		git checkout master && git push; \
		else >&2 echo "currently deploying, wait until finished" && exit 1; \
		fi

.PHONY: push-prod
push-prod:
	@if [ -z "$$(drone-cli.sh build list ${DRONE_REPO} --status running)" ]; then \
		git checkout master && git push; \
		else >&2 echo "currently deploying to stage or production, wait until finished" && exit 1; \
		fi
	@echo
	@if ! git diff --exit-code origin/master..master > /dev/null 2>&1; then >&2 echo "deploy changes to staging first! (push to master branch)!"; exit 1; fi
	@echo
	@git diff prod..master
	@read -p "Deploy the above changes to PRODUCTION? (merge prod w/ master and push) [Y/n]: " response && \
		case "$$response" in \
			[yY]) git checkout prod && git merge master && git push && git checkout master;; \
			?) echo "NOT deploying changes";; esac
