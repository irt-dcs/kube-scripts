.PHONY: sign
sign: add-drone-repo ## sign .drone.yml
	@drone-cli.sh sign ${DRONE_REPO}

.PHONY: add-drone-repo
add-drone-repo: update-scripts ## add repo to drone
	@drone-add-repo.sh

.PHONY: rm-drone-repo
rm-drone-repo: update-scripts ## remove repo from drone
	@drone-rm-repo.sh

.PHONY: ls-sec
ls-sec: ## list CI/CD secrets
	@drone-cli.sh secret ls ${DRONE_REPO}

.PHONY: add-default-sec
add-default-sec: ## add default set of CI/CD secrets
	@cat ${REGISTRY_PASSWORD_FILE} | tr '\r\n' ' ' > ${DRONE_CLI_WORKDIR}/.registry_key
	@drone-cli.sh secret add --conceal ${DRONE_REPO} REGISTRY_PASSWORD @.registry_key
	@drone-cli.sh secret add ${DRONE_REPO} REGISTRY ${REGISTRY}
	@drone-cli.sh secret add ${DRONE_REPO} REGISTRY_USERNAME ${REGISTRY_USERNAME}
	@drone-cli.sh secret add ${DRONE_REPO} CHANNEL ${CHANNEL}
	@drone-cli.sh secret add --conceal ${DRONE_REPO} DRONE_TOKEN @${DRONE_TOKEN_FILE}

.PHONY: drone-web
drone-web: ## open ${DRONE_REPO_ADDRESS} in web browser
	@open ${DRONE_REPO_ADDRESS}

.PHONY: rm-sec
rm-sec: ## remove all CI/CD secrets
	@for s in $$(drone-cli.sh secret ls --format '{{ .Name }}' ${DRONE_REPO}); \
		do drone-cli.sh secret rm ${DRONE_REPO} $$s; \
	done

.PHONY: drone-logs
drone-logs: ## show logs from latest drone build
	@drone-cli.sh build logs ${DRONE_REPO} \
		$$(drone-cli.sh build last ${DRONE_REPO} --format '{{ .Number }}')

.PHONY: drone-builds
drone-builds: ## list CI/CD builds
	@drone-cli.sh build list ${DRONE_REPO}

.PHONY: check-prod-branch-not-ahead
check-prod-branch-not-ahead: ## assert that remote prod branch isn't ahead of remote master branch (for CI)
	@git fetch && if ! [ "" = "$$(git rev-list origin/master..origin/prod)" ]; then echo 'ERROR, origin/prod branch ahead of origin/master!'; exit 1; fi
