#!/bin/bash

if empty_var CLOUDSDK_CONTAINER_USE_CLIENT_CERTIFICATE; then
  echo
  echo WARNING: env var \$CLOUDSDK_CONTAINER_USE_CLIENT_CERTIFICATE should be set to True in most cases
  echo
fi
