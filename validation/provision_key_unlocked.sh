#!/bin/bash

if [ ! -f $GCP_KEY_FILE ]; then
  err $GCP_KEY_FILE does not exist!
fi

file $GCP_KEY_FILE | grep -q data
if [ $? -eq 0 ]; then
   err $GCP_KEY_FILE is locked! do \'git-crypt unlock\'.
fi
