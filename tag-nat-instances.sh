#!/bin/bash

##########################################################
# Tag all instances with a tag $TAG or 'stanford'
# Used to tag instances for nat routing
##########################################################

# TAG defaults to stanford
TAG=${1:-$GCP_NAT_PREFIX}

# Tag instances for NAT routing (excluding the nat server itself)
gcloud compute instances list \
	--zones ${GCP_ZONE} \
  --filter='NOT tags.items:stanford AND NOT tags.items:nat-server AND status:RUNNING' \
  --format json \
   | jq -r '.[].name' \
   | xargs -I{} gcloud compute instances add-tags {} --tags ${TAG}
