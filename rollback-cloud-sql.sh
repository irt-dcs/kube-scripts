#!/bin/bash

###############################################################################
# Creates a cloud sql instance
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

instance_prefix=$1
tier=$2
storage_size=$3
mysql_version=$4
failover_instance_prefix=$5
region=$6
failover_zone=$7

instance_name=$($THIS_DIR/cloud-sql-prefix-to-name.sh $instance_prefix)
old_failover_instance_name=$($THIS_DIR/cloud-sql-prefix-to-name.sh $failover_instance_prefix)
new_failover_instance_name="${failover_instance_prefix}-$(date +%s)"

# first delete the failover replica
echo
echo "restoring master Cloud SQL instance '$instance_name' to the following backup:"
backup_object=$(gcloud --format json beta sql backups list --instance $instance_name \
	 	 --sort-by windowStartTime | jq -ec .[-1])
echo $backup_object | jq
echo

echo "PRE-ROLLBACK: deleting Cloud SQL FAILOVER replica instance '${old_failover_instance_name}' to rollback master instance"
gcloud sql instances delete $old_failover_instance_name

echo "ROLLBACK: restoring master Cloud SQL instance '${instance_name}' to backup:"
backup_id=$(echo $backup_object | jq -er .id)
gcloud beta sql backups restore --restore-instance=$instance_name $backup_id

echo "POST-ROLLBACK: recreating failover replica instance w/ new name '${new_failover_instance_name}'"
gcloud beta sql instances create \
  --region $region \
  --database-version $mysql_version \
  --gce-zone $failover_zone \
  --tier $tier \
  --storage-auto-increase \
  --storage-size $storage_size \
  --master-instance-name $instance_name \
  --replica-type FAILOVER \
  $new_failover_instance_name
