#!/bin/bash

###############################################################################
# Snapshot a cloud sql instance
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

instance_prefix=$1
description=$2

instance_name=$($THIS_DIR/cloud-sql-prefix-to-name.sh $instance_prefix)

gcloud beta sql backups create --instance $instance_name --description "$description"
