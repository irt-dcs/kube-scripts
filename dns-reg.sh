#!/bin/bash

###############################################################################
# Register zone name IP
###############################################################################

TRAN_FILE=/tmp/transaction.yaml
ZONE=$1  # e.g. example-zone
NAME=$2
IP=${3:-${SERVICE_IP}}

# NAME must ended with '.'
if [ "${NAME:$((${#str}-1)):1}" != '.' ]
then
  NAME="${NAME}."
fi

EXISTING_IP=$(gcloud dns record-sets list -z ${ZONE} | grep ${NAME} | awk '{print $4}' 2>/dev/null)
if [ -z ${EXISTING_IP} ]
then
  echo DNS register zone=${ZONE} name=${NAME} ip=${IP}
  gcloud dns record-sets transaction start -z ${ZONE} --transaction-file ${TRAN_FILE}
  gcloud dns record-sets transaction add -z ${ZONE} --transaction-file ${TRAN_FILE} --name ${NAME} --ttl 60 --type A ${IP}
  #gcloud dns record-sets transaction describe -z ${ZONE} --transaction-file ${TRAN_FILE}
  gcloud dns record-sets transaction execute -z ${ZONE} --transaction-file ${TRAN_FILE}
  if [ -f ${TRAN_FILE} ] ; then rm -f ${TRAN_FILE} ; fi
elif [ ${EXISTING_IP} != ${IP} ]
then
  echo change DNS register zone=${ZONE} name=${NAME} from {EXISTING_IP} to ${IP}
  gcloud dns record-sets transaction start -z ${ZONE} --transaction-file ${TRAN_FILE}
  gcloud dns record-sets transaction remove -z ${ZONE} --transaction-file ${TRAN_FILE} --name ${NAME} --ttl 60 --type A ${EXISTING_IP}
  gcloud dns record-sets transaction add -z ${ZONE} --transaction-file ${TRAN_FILE} --name ${NAME} --ttl 60 --type A ${IP}
  #gcloud dns record-sets transaction describe -z ${ZONE} --transaction-file ${TRAN_FILE}
  gcloud dns record-sets transaction execute -z ${ZONE} --transaction-file ${TRAN_FILE}
  if [ -f ${TRAN_FILE} ] ; then rm -f ${TRAN_FILE} ; fi
else
  echo name=${NAME} already registered.
fi
