#!/bin/bash

###############################################################################
# Delete firewall rules if they exist
###############################################################################

GKE_CLUSTER_NAME=${1:-$GKE_CLUSTER_NAME}

if gcloud compute firewall-rules describe ${GKE_CLUSTER_NAME}-ingress-check > /dev/null 2>&1
then
  # remove allow-ingress-check
  gcloud compute firewall-rules delete ${GKE_CLUSTER_NAME}-ingress-check --quiet
else
  echo "Firewall ${GKE_CLUSTER_NAME}-ingress-check: not found"
fi
