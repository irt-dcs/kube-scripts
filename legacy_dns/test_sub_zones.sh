#!/bin/bash
# NOT INFRASTRUCTURE, JUST A TEST TO VERIFY BEHAVIOR OF CREATING ZONES WITH
# SHARED DNS SEGMENTS, e.g. a.b.com. and x.a.b.com. NEVER COLLIDE ON SAME NS
# however, b.com and a.com will collide on same namserver
# RESULT: gcloud dns selects your nameservers, but it is smart

> nameservers.txt

gcloud dns managed-zones delete main
gcloud dns managed-zones create --dns-name=a.b.com. --description=blah main
main_nameserver=$(gcloud --format json dns managed-zones describe main | jq -r .nameServers[0])

for i in {1..500}; do
  gcloud dns managed-zones delete sub
  gcloud dns managed-zones create --dns-name=x.a.b.com. --description=blah sub
  sub_nameserver=$(gcloud --format json dns managed-zones describe sub | jq -r .nameServers[0])

  echo $main_nameserver $sub_nameserver >> nameservers.txt
  if [ "$main_nameserver" = "$sub_nameserver" ]; then
    echo "ERROR: collision of nameservers!"
    exit
  fi
done
