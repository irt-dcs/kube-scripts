#!/usr/local/bin/python3
# A script which uses pyparsing to parse (a specific subset of) tinydns data
# files and output in the Google's Cloud DNS Records Format:
# like https://cloud.google.com/dns/records/json-record but YAML instead
#
# It is heavily tailored to some specific tinydns data files, but could be
# extended to parse the entire tinydns data format
#
# CAVEATS:
# * doesn't properly combine A records with same name to have multiple IPs
#   ** so fixed this manually after output


from os.path import join
import os
from collections import defaultdict
from argparse import ArgumentParser
from pyparsing import Word, Suppress, Literal, Combine, Optional
from pyparsing import Regex, nums, alphanums, printables
import yaml

# Exclude DNS entry types from being exported since Google puts in defaults
EXCLUDE_TYPES = [
    # We can ignore NS entries, as google by default puts in these entries
    'NS',
    # Also don't export SOA entries, just use google defaults
    # We do use the SOA entries however to define the zones
    'SOA',
    # We ignore PTR records since we aren't handling reverse DNS
    'PTR'
]


# CONSTANTS FOR Changing SOA record
# got from looking up settings from irt-dns-01.stanford.edu
# dig +noall +answer SOA irt-dr.med.stanford.edu
DEFAULT_SERIAL = 1481694891
DEFAULT_REFRESH_TIME = 16384
DEFAULT_RETRY_TIME = 2048
DEFAULT_EXPIRY_TIME = 1048576
DEFAULT_MINIMUM_TIME = 300

# Defaults for MX records
# based on doing dig of MX records w/out TTL
# dig +noall +answer MX ccipp02.irt-dr.med.stanford.edu
MX_DEFAULT_TTL = 86400

SOA_FORMAT = "{master_name} {email} {serial} {refresh} {retry} {expiry}\
 {minimum}"

# preference is same thing as distance
MX_FORMAT = "{preference} {host}"


# HELPERS
def ensure_period(string):
    """make sure period at end of string

    >>> ensure_period('asdf.')
    'asdf.'
    >>> ensure_period('asdf')
    'asdf.'
    """
    return string if string[-1] == '.' else string + '.'


def reverse_ip(ip_addr):
    return '.'.join(reversed(ip_addr.split('.')))


def name_to_zone(name, available_zones):
    """choose appropriate zone for DnsEntry name from a list of available zones
    the general algorithm is to select the longest matching zone name

    # simple matching
    >>> name_to_zone('a.com.', ['b.com.', 'a.com.'])
    'a.com.'

    # pick the longest
    >>> name_to_zone('c.b.a.com.', ['a.com.', 'b.a.com.'])
    'b.a.com.'

    # non-matching
    >>> name_to_zone('z.z.com', ['a.com'])
    Traceback (most recent call last):
     ...
    LookupError: no matching zone for "z.z.com" in ['a.com']
    """

    longest_matching_zone = ''
    for zone in available_zones:
        if name.rfind(zone) != -1 and len(zone) > len(longest_matching_zone):
            longest_matching_zone = zone

    if longest_matching_zone:
        return longest_matching_zone
    else:
        raise LookupError('no matching zone for "%s" in %s' %
                          (name, available_zones))


def zone_to_zone_name(zone):
    """convert "zone" to Google Cloud DNS "zone name"

    # . -> -dot-
    >>> zone_to_zone_name('a.com.')
    'a-dot-com-dot'

    # _ -> -unsc-
    >>> zone_to_zone_name('a_a.com.')
    'a-unsc-a-dot-com-dot'
    """

    zone_name = zone.replace('.', '-dot-').replace('_', '-unsc-')
    # rid of trailing -
    if zone_name[-1:] == '-':
        zone_name = zone_name[:-1]

    return zone_name


def zone_name_to_zone(zone_name):
    """convert Google Cloud DNS "zone name" to "zone"

    # -dot- -> .
    >>> zone_name_to_zone("a-dot-com-dot")
    'a.com.'

    # -unsc- -> _
    >>> zone_name_to_zone("a-unsc-a-dot-com-dot")
    'a_a.com.'
    """

    replacements = {
        '-dot-': '.',
        '-unsc-': '_',
        '-dot': '.'
    }

    zone = zone_name

    for old, new in replacements.items():
        zone = zone.replace(old, new)

    return zone


class DnsEntry(object):
    """DNS Entry object google expects for `gcloud dns record-sets import`"""

    def __init__(self, name, rrdatas, ttl, entry_type, kind=None):

        if kind:
            self.kind = kind
        else:
            self.kind = "dns#resourceRecordSet"

        self.name = name
        self.rrdatas = rrdatas
        self.ttl = ttl
        self.type = entry_type

    def __repr__(self):
        return str(self.__dict__)

    def __iter__(self):
        return self.__dict__

    def to_yaml(self):
        return yaml.dump(self.__iter__(), default_flow_style=False)

    def zone(self, available_zones):
        return name_to_zone(self.name, available_zones)


class Comment(object):
    """Entry to record comments"""

    def __init__(self, text):
        self.text = text

    def to_yaml(self):
        return "#" + self.text

    def __repr__(self):
        return self.text


# general grammars
integer = Word(nums).setParseAction(
    lambda s, l, t: int(t[0])).setName("integer")
colon = Suppress(Literal(":")).setName(":")
# return hostname with period after
hostname = Word(alphanums + '.' + '-' + '*').setParseAction(
    lambda s, l, t: ensure_period(t[0])).setName("hostname")
ip_addr = Combine(Word(nums) + ('.' + Word(nums)) * 3).setName("ip_addr")
anything = Regex('.*')
anything_but_colon = Word(printables.replace(
    ':', '')).setName("anything_but_colon")

# tiny dns segment grammars
# not 100% to spec
fqdn = hostname.setResultsName("fqdn")
ip = ip_addr.setResultsName("ip")
host = hostname.setResultsName("host")
ttl = integer.setResultsName("ttl")
distance = Optional(integer).setResultsName("distance")
s_text = anything_but_colon.setResultsName("s_text")
pointer = hostname.setResultsName("pointer")
master_name = hostname.setResultsName("master_name")
role_name = hostname.setResultsName("role_name")
min_time = Optional(integer).setResultsName("min_time")


# TINY DNS LINE GRAMMARS

# COMMENT
comment_format = Suppress(Literal("#")) + \
    Optional(anything).setResultsName("text")


def comment_transformer(_, __, t):
    """a setParseAction that returns a Comment from a tokenized tinydns
    # (comment) line

    t is tokens Dictionary
    """

    return [
        Comment(t['text'])
    ]


comment_parser = comment_format.setParseAction(comment_transformer)

# Z => SOA
# format=> Zfqdn:mname:rname:ser:ref:ret:exp:min:ttl:timestamp:lo
# required=> fqdn:mname:rname
soa_format = Suppress(Literal("Z")) + fqdn + colon + master_name + colon +  \
    role_name + colon * 5 + min_time + colon + ttl + colon * 2


def soa_transformer(_, __, t):
    """a setParseAction function that returns a SOA DnsEntry object from
    tokenized tinydns Z (SOA) line

    t is tokens Dictionary
    """

    soa_string = SOA_FORMAT.format(
        master_name=t['master_name'],
        email=t['role_name'],
        serial=DEFAULT_SERIAL,
        refresh=DEFAULT_REFRESH_TIME,
        retry=DEFAULT_RETRY_TIME,
        expiry=DEFAULT_EXPIRY_TIME,
        minimum=t['min_time']
    )

    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[soa_string],
                 ttl=t['ttl'],
                 entry_type='SOA')
    ]


soa_parser = soa_format.setParseAction(soa_transformer)


# . => NS+A+SOA  format=> .fqdn:ip:host:ttl:timestamp:lo  required=> fqdn
# we only parse this subset of . lines:
#    .panic.mil::a.ns.heaven.af.mil
#
# which is enough for NS + SOA, but we throw away the SOA since we use Z lines
ns_soa_format = Suppress(Literal(".")) + fqdn + colon * 2 + host + colon  \
    + ttl


def ns_soa_transformer(_, __, t):
    """a setParseAction function that returns a NS DnsEntry object from
    tokenized tinydns . (NS_A_SOA) line

    We have no need to make SOA DnsEntry since we always do Z entries

    t is tokens Dictionary
    """

    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[t['host']],
                 ttl=t['ttl'],
                 entry_type='NS')
    ]


ns_soa_parser = ns_soa_format.setParseAction(ns_soa_transformer)


# C => CNAME    format=> Cfqdn:p:ttl:timestamp:lo  required=> fqdn:p
cname_format = Suppress(Literal("C")) + fqdn + colon + pointer + colon + ttl


def cname_transformer(_, __, t):
    """a setParseAction function that returns a CNAME DnsEntry object from
    tokenized tinydns C (CNAME) line

    We have no need to make SOA DnsEntry since we always do Z entries

    t is tokens Dictionary
    """
    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[t['pointer']],
                 ttl=t['ttl'],
                 entry_type='CNAME')
    ]


cname_parser = cname_format.setParseAction(cname_transformer)

# = => A+PTR    format=> =fqdn:ip:ttl:timestamp:lo  required=> fqdn:ip
# we only use fqdn:ip:ttl
a_ptr_format = Suppress(Literal("=")) + fqdn + colon + ip + colon + ttl


def a_ptr_transformer(_, __, t):
    """a setParseAction function that returns an A and PTR DnsEntry object from
    tokenized tinydns = (A + PTR) line

    t is tokens Dictionary
    """

    ptr_addr = reverse_ip(t['ip']) + ".in-addr.arpa."
    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[t['ip']],
                 ttl=t['ttl'],
                 entry_type='A'),
        DnsEntry(name=ptr_addr,
                 rrdatas=[t['fqdn']],
                 ttl=t['ttl'],
                 entry_type='PTR')
    ]


a_ptr_parser = a_ptr_format.setParseAction(a_ptr_transformer)

# ' => TXT      fqdn:s:ttl:timestamp:lo  required=> fqdn:s
# we only use fqdn:s:ttl
txt_format = Suppress(Literal("'")) + fqdn + colon + s_text + colon + ttl


def txt_transformer(_, __, t):
    """a setParseAction function that returns an TXT DnsEntry object from
    tokenized tinydns ' TXT line

    t is tokens Dictionary
    """

    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[t['s_text']],
                 ttl=t['ttl'],
                 entry_type='TXT')
    ]


txt_parser = txt_format.setParseAction(txt_transformer)

# @ => MX+A       format=> @fqdn:ip:host:dist:ttl:timestamp:lo  required=> fqdn
# we only use MX (not A) so @fqdn::host:ttl
mx_a_format = Suppress(Literal("@")) + fqdn + colon * 2 + host + colon \
    + distance + Optional(colon) + Optional(ttl)


def mx_a_transformer(_, __, t):
    """a setParseAction function that returns an MX DnsEntry object from
    tokenized tinydns @ MX + A line. We don't utilize A record functionality.

    t is tokens Dictionary
    """

    # sometimes TTL is not set in our case
    ttl = t['ttl'] if 'ttl' in t else MX_DEFAULT_TTL

    # google uses this format for rrdatas entry
    mx_string = MX_FORMAT.format(
        preference=t['distance'],
        host=t['host']
    )

    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[mx_string],
                 ttl=ttl,
                 entry_type='MX')
    ]


mx_a_parser = mx_a_format.setParseAction(mx_a_transformer)

# + => A        format=> +fqdn:ip:ttl:timestamp:lo  required=> fqdn:ip
# we only use fqdn:ip:ttl
a_format = Suppress(Literal("+")) + fqdn + colon + ip + colon + ttl


def a_transformer(_, __, t):
    """a setParseAction function that returns an A DnsEntry object from
    tokenized tinydns + (A) line.

    t is tokens Dictionary
    """

    return [
        DnsEntry(name=t['fqdn'],
                 rrdatas=[t['ip']],
                 ttl=t['ttl'],
                 entry_type='A')
    ]


a_parser = a_format.setParseAction(a_transformer)


def parse_tiny_dns_file(tiny_dns_file, output_directory):

    # go through to find matching parser
    line_parser = comment_parser | soa_parser | ns_soa_parser | \
        cname_parser | a_ptr_parser | txt_parser | mx_a_parser | a_parser

    zones = ['in-addr.arpa.']
    line_objects = []
    with open(tiny_dns_file, 'r') as f:
        for num, line in enumerate(f):

            # only parse non-empty lines, also ignore empty comment
            if line.replace('#', '').strip():
                for line_object in line_parser.parseString(line):
                    if isinstance(line_object, DnsEntry):
                        dns_object = line_object
                        if dns_object.type == 'SOA':
                            zones.append(dns_object.name)

                    line_objects.append(line_object)

    return line_objects, zones


def filter_comments(line_objects):
    for line_object in line_objects:
        if not isinstance(line_object, Comment):
            yield line_object


def filter_dns_entries(dns_entries):
    for dns_entry in dns_entries:
        if dns_entry.type not in EXCLUDE_TYPES:
            yield dns_entry


def filter_lines(line_objects):
    return filter_dns_entries(filter_comments(line_objects))


def create_zone_dict(line_objects, zones):
    zone_dict = defaultdict(list)
    for dns_entry in filter_lines(line_objects):
        zone = dns_entry.zone(zones)
        zone_dict[zone].append(dns_entry)

    return dict(zone_dict)


def write_zone_files(zone_dict, output_directory):
    for zone, entries in zone_dict.items():
        zone_name = zone_to_zone_name(zone)
        zone_file = join(output_directory, zone_name + '.yml')

        # remove file if exists
        try:
            os.remove(zone_file)
        except OSError:
            pass

        # write, appending data
        with open(zone_file, 'a') as f:
            first = True
            for dns_entry in entries:
                if first:
                    first = False
                else:
                    f.write('---\n')

                f.write(dns_entry.to_yaml())


def convert(tiny_dns_file, output_directory):
    line_objects, zones = parse_tiny_dns_file(tiny_dns_file, output_directory)
    zone_dict = create_zone_dict(line_objects, zones)
    write_zone_files(zone_dict, output_directory)


def main():
    parser = ArgumentParser(
        description='Convert tiny dns entries files to Google Cloud DNS yaml')
    parser.add_argument('tiny_dns_file', action="store")
    parser.add_argument('output_directory', action="store")
    args = parser.parse_args()
    convert(args.tiny_dns_file, args.output_directory)


if __name__ == '__main__':
    main()
