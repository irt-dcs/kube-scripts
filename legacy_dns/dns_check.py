#!/usr/bin/env python

from __future__ import print_function
import sys
from os import listdir
from os.path import join, basename, splitext
from argparse import ArgumentParser
from subprocess import check_output
import yaml


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def check_all_items_identical(iterator):
    return len(set(iterator)) <= 1


def remove_trailing_period(string):
    """remove a trailing period if exists

    >>> remove_trailing_period("stuff.")
    'stuff'
    >>> remove_trailing_period("stuff.")
    'stuff'
    """
    return string[:-1] if string[-1] == '.' else string


def dig(name, record_type, dns_server):
    output = check_output(dig_command(name, record_type, dns_server))

    # normalize whitespace on each line, sort lines and join back into string
    return output


def normalize_dig_output(output):
    """normalize whitespace in and lowercase line and sort lines

    >>> normalize_dig_output( \
    'domain 300\tIN A 000.000.000.13\\nDomain\t300 IN A 000.000.000.12')
    'domain 300 in a 000.000.000.12\\ndomain 300 in a 000.000.000.13'

    """
    return '\n'.join(
        sorted([' '.join(line.lower().split())
                for line in output.strip().splitlines()])
    )


def dig_command(name, record_type, dns_server):
    return ['dig', '+noall', '+answer', name, record_type, '@' + dns_server]


def get_dns_entries(entries_file):
    with open(entries_file, 'r') as f:
        for dns_entry in yaml.load_all(f):
            yield dns_entry


def compare_dns_entries(entries_file, dns_servers):
    for dns_entry in get_dns_entries(entries_file):
        compare_dns_entry(dns_entry, dns_servers)


def verify_dns_entries(entries_file, dns_server):
    for dns_entry in get_dns_entries(entries_file):
        emulated_dig_out = normalize_dig_output(emulate_dig_output(dns_entry))

        real_dig_out = normalize_dig_output(
            dig(dns_entry['name'], dns_entry['type'], dns_server)
        )

        if emulated_dig_out != real_dig_out:
            eprint("FAILURE: dig outputs not identical for query:")
            eprint("expected dig output:\n%s" % emulated_dig_out)
            eprint("actual dig output:\n%s" % real_dig_out)
            exit(1)

def zone_name_from_filename(entries_file):
    """give zone_name for an entries yml file

    >>> zone_name_from_filename('stuff/zone_name.yml')
    'zone_name'
    """
    return splitext(basename(entries_file))[0]


def get_google_nameserver(zone_name):
    output_yaml = check_output(
        ['gcloud', 'dns', 'managed-zones', 'describe', zone_name])

    # just return the first NS listed, should be sufficient
    nameserver = yaml.load(output_yaml)['nameServers'][0]

    # get rid of period
    return remove_trailing_period(nameserver)


def compare_dns_entry(entry, dns_servers):
    """compare dig output on two or more dns servers

    entry has at least these items:
    {'type': 'CNAME', 'name': 'a.hostname.'}
    example:
    {'type': 'CNAME', 'name': 'a.hostname.'}
    """
    name = entry['name']
    dns_type = entry['type']
    output_dict = {' '.join(dig_command(name, dns_type, s)):
                   normalize_dig_output(dig(name, dns_type, s))
                   for s in dns_servers}

    def print_outputs(output_dict):
        for command, output in output_dict.iteritems():
            eprint('$ %s' % command)
            if output.strip():
                eprint('%s' % output)
            else:
                eprint('--NO OUTPUT--')

    # verify that outputs from dig are identical
    if not check_all_items_identical(output_dict.values()):
        eprint("FAILURE: dig outputs not identical for query:")
        print_outputs(output_dict)
        exit(1)

    elif any(not output.strip() for output in output_dict.itervalues()):
        eprint("WARNING: one or more dig outputs empty")
        print_outputs(output_dict)


def compare_dns_entries_dir(entries_directory, legacy_dns_server):
    for entries_file in listdir(entries_directory):
        zone_name = zone_name_from_filename(entries_file)
        dns_servers = [legacy_dns_server, get_google_nameserver(zone_name)]
        compare_dns_entries(join(entries_directory, entries_file), dns_servers)

    print("verified dig output of Google Cloud DNS matches dig output of '{}'"\
          .format(entries_directory))


def emulate_dig_output(entry):
    """emulate dig output from a google DNS entry object

    A record
    >>> entry = {'name': 'stuff.', 'ttl': 123, 'type': 'A', 'rrdatas': ['1.1.1.1',
    ... '1.2.3.4']}
    >>> emulate_dig_output(entry)
    'stuff. 123 in A 1.1.1.1\\nstuff. 123 in A 1.2.3.4\\n'

    TXT record
    >>> entry = {'name': 'stuff.', 'ttl': 123, 'type': 'A', 'rrdatas': ['1.1.1.1',
    ... '1.2.3.4']}
    >>> emulate_dig_output(entry)
    'stuff. 123 in A 1.1.1.1\\nstuff. 123 in A 1.2.3.4\\n'
    """
    dig_format = "{name} {ttl} in {type} {data}"
    return '\n'.join(
        dig_format.format(
            name=entry['name'],
            ttl=str(entry['ttl']),
            type=entry['type'],
            data='"%s"' % data if entry['type'] == 'TXT' else data
        ) for data in entry['rrdatas']
    ) + '\n'


def verify_dns_entries_dir(entries_directory):
    for entries_file in listdir(entries_directory):
        zone_name = zone_name_from_filename(entries_file)
        dns_server = get_google_nameserver(zone_name)

        verify_dns_entries(join(entries_directory, entries_file), dns_server)

    print("verified dig output of Google Cloud DNS matches entries defined in "
          + entries_directory)


def main():

    parser = ArgumentParser(description='do black box comparison of DNS servers')

    subparsers = parser.add_subparsers(help='commands')

    compare_parser = subparsers.add_parser(
        'compare',
        help='do black-box comparison of gcloud and legacy servers')
    compare_parser.add_argument('entries_directory')
    compare_parser.add_argument('legacy_dns_server')
    compare_parser.set_defaults(command='compare')

    verify_parser = subparsers.add_parser(
        'verify',
        help='do black-box verification of gcloud according to yaml zone files')
    verify_parser.set_defaults(command='verify')
    verify_parser.add_argument('entries_directory')

    args = parser.parse_args()

    if args.command == 'compare':
        compare_dns_entries_dir(args.entries_directory, args.legacy_dns_server)
    elif args.command == 'verify':
        verify_dns_entries_dir(args.entries_directory)


if __name__ == '__main__':
    main()
