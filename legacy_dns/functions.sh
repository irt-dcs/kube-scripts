#!/bin/bash
# Shared functions used by

# merge two DNS entries files directories to one output directory, cat'ing same-named files together
# -- useful for combining two sets of YAML zone (entries) files
# -- like when we combine main.normal with lane.failover (or any permutation)
#
# example 1:
# merge_entries_files dir1 dir2 output_dir
#   dir1/
#     file1.yml
#     file2.yml
#   dir2/
#     file1.yml
#     file3.yml
# ---->
#   output_dir/
#     file1.yml (dir1/file1.yml and dir2/file1.yml cat'ed together with '---')
#     file2.yml (dir1/file2.yml)
#     file3.yml (dir2/file3.yml)
#
merge_entries_files() {
  local dir1=$1
  local dir2=$2
  local output_dir=$3

  # remove output_dir if exists
  rm -rf $output_dir

  # copy dir1 to output dir
  cp -r $dir1 $output_dir

  #  merge dir2 files with the dir1 files
  for f in $(ls $dir2); do
    if [ -f $output_dir/$f ]; then
      echo merging $dir1/$f with $dir2/$f
      echo '---' >> $output_dir/$f
    fi
    cat $dir2/$f >> $output_dir/$f
  done
}

# convert Google Cloud DNS "Zone name" to "DNS name" from our naming convention
# example
# input: myname-dot-domain-dot-com-dot
# output: myname.domain.com.
zone_name_to_dns_name() {
  local intermediate=${1//-dot-/.}

  # replace the special -dot at the end (no trailing -)
  echo ${intermediate//-dot/.}
}
