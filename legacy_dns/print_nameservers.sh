#!/bin/bash
# prints DNS nameservers

set -e

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $THIS_DIR/functions.sh

ENTRIES_DIR1=$1
ENTRIES_DIR2=$2

TEMP_DIR=.temp

# delete zones
print_nameservers() {
  local dns_entries_dir=$1

  for yaml_file in $(ls $dns_entries_dir); do
    yaml_path=$dns_entries_dir/$yaml_file
    zone_name=$(basename ${yaml_file/.yml/})
    dns_name=$(zone_name_to_dns_name $zone_name)

    echo "zone \"$zone_name\" ($dns_name):"
    gcloud dns managed-zones describe $zone_name --format json | jq -r '.nameServers[]' | sed 's/^/  /'
  done
}

main() {
  merge_entries_files $ENTRIES_DIR1 $ENTRIES_DIR2 $TEMP_DIR
  print_nameservers $TEMP_DIR
}

main

