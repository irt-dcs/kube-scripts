#!/bin/bash
# merge two directories of YAML zone files, and create zones and entries defined by the files

set -e

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $THIS_DIR/functions.sh

ENTRIES_DIR1=$1
ENTRIES_DIR2=$2

TEMP_DIR=.temp

print_dns_diff() {
  json_output_file=$1

  # when no changes, json will be []
  if cat $1 | jq '.id' > /dev/null 2>&1; then
    echo "ADDITIONS:"
    cat $1 | jq '.additions[]' | grep -v '{' | tr '}' '_' | sed 's/_/---/g'
    echo "DELETIONS:"
    cat $1 | jq '.deletions[]' | grep -v '{' | tr '}' '_' | sed 's/_/---/g'
  fi
}

apply_entries_files() {
  local dns_entries_dir=$1

  for yaml_file in $(ls $dns_entries_dir); do
    yaml_path=$dns_entries_dir/$yaml_file
    zone_name=$(basename ${yaml_file/.yml/})
    dns_name=$(zone_name_to_dns_name $zone_name)

    if ! gcloud dns managed-zones describe $zone_name > /dev/null 2>&1; then
      gcloud dns managed-zones create --dns-name=$dns_name $zone_name \
        --description='DO NOT add/modify entries in this zone! automatically provisioned, all changes will be DESTROYED'
    fi

    gcloud --format json dns record-sets import --delete-all-existing --zone=$zone_name $yaml_path > /tmp/prov_dns.json
    print_dns_diff /tmp/prov_dns.json
    rm /tmp/prov_dns.json
  done
}

main() {
  merge_entries_files $ENTRIES_DIR1 $ENTRIES_DIR2 $TEMP_DIR
  apply_entries_files $TEMP_DIR
}

main
