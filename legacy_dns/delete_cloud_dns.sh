#!/bin/bash
# merge two directories of YAML zone files, and delete all zones (and all contained entries) defined by files

set -e

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $THIS_DIR/functions.sh

ENTRIES_DIR1=$1
ENTRIES_DIR2=$2

TEMP_DIR=.temp

# delete zones
delete_zones_from_entries_files() {
  local dns_entries_dir=$1

  for yaml_file in $(ls $dns_entries_dir); do
    yaml_path=$dns_entries_dir/$yaml_file
    zone_name=$(basename ${yaml_file/.yml/})

    # delete all entries in zone (zone has to be empty before deletion)
    gcloud dns record-sets import --zone $zone_name --delete-all-existing /dev/null

    # delete managed-zone
    gcloud dns managed-zones delete $zone_name
  done
}

main() {
  merge_entries_files $ENTRIES_DIR1 $ENTRIES_DIR2 $TEMP_DIR
  delete_zones_from_entries_files $TEMP_DIR
}

main
