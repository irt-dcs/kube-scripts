#!/bin/bash

##########################################################
# Tag all nodes in the kubernetes with $TAG or 'stanford'
# Used to tag node for nat routing
##########################################################

# TAG defaults to stanford
TAG=${1:-$GCP_NAT_PREFIX}
# TODO: make work for multiple additional zones
ADDITIONAL_ZONE=${2:-$GCP_NAT_ADDITIONAL_ZONE}

for n in $(kubectl get nodes -o 'jsonpath={.items[*].metadata.name}')
do
  if [ -z ${ADDITIONAL_ZONE} ]; then
    gcloud compute instances add-tags $n --tags ${TAG}
  else
    # TODO make work for multiple additional zones
    # gcloud needs zone specified, so for now just use additional zone if instance is not in main zone
    if gcloud compute instances describe $n > /dev/null 2>&1; then
      echo "tag '$n' with '${TAG}'"
      gcloud compute instances add-tags $n --tags ${TAG}
    else
      echo "tag '$n' with '${TAG}' in alternate zone '${GCP_NAT_ADDITIONAL_ZONE}'"
      gcloud compute instances add-tags $n --tags ${TAG} --zone ${GCP_NAT_ADDITIONAL_ZONE} 
    fi
  fi
done
