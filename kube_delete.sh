#!/bin/bash

###############################################################################
# delete resources from list of input templates
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh
source env.sh

# fail on error or undeclared vars
trap_errors

template_files=$@

echo deleting kubernetes resources with templates:
for template in $template_files; do
  echo "   $template"
  # don't fail if resource exists
  cat $template | envsubst | kubectl delete -f - || true
done
