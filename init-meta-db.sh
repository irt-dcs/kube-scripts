#!/bin/bash

###############################################################################
# Import the idpproxy metadata to PDO DB
# Note: importPdoMetadata.php will create all the tables if don't exist
###############################################################################

APP=$1
POD=$(kubectl get pod -l app=${APP} -o json | jq -r '.items[0].metadata.name')

# exec importPdoMetadata.php
echo "kubectl exec -i ${POD} php bin/importPdoMetadata.php"
kubectl exec -i ${POD} php bin/importPdoMetadata.php
