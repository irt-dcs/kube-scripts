#!/bin/bash

###############################################################################
# Check that there aren't obvious problems with config, etc...
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh

# validate that there aren't typical issues
for f in $THIS_DIR/validation/*.sh; do
  try $f
done
