#!/bin/bash

###############################################################################
# Create a persistent disk
###############################################################################

# Usage: $0 <disk name> <zone> <size> <type>

# get command-line input, set default parameters
DISK_NAME=${1:-$DISK_NAME}
DISK_SIZE=${2:-$DISK_SIZE}
DISK_TYPE=${3:-pd-standard}
ZONE=${4:-$GCP_ZONE}

# exit if disk already created
if gcloud compute disks describe ${DISK_NAME} --zone ${ZONE} &> /dev/null
then
  echo ${DISK_NAME} exists, do nothing
  exit
fi

# create disk
gcloud compute disks create ${DISK_NAME} --zone ${ZONE} --size ${DISK_SIZE} --type ${DISK_TYPE}
