#!/bin/bash

###############################################################################
# Creates a cloud sql instance
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

instance_prefix=$1
tier=$2
storage_size=$3
mysql_version=$4
backup_time=$5
failover_instance_prefix=$6
region=$7
zone=$8
failover_zone=$9
database_name=${10}
database_user=${11}
database_password_file=${12}
  [ -f $database_password_file ] || die 'database_password_file is not a file'
root_password_file=${13}
  [ "$root_password_file" == 'none' ] || [ -f $root_password_file ] \
    || die "root_password_file is not 'none' or a file"

# is there a cloud sql instance with given prefix?
# cloud sql prefix to name returns err code 1 for not exists, 2 for more than one SQL instance matching
err_code=0
$THIS_DIR/cloud-sql-prefix-to-name.sh $instance_prefix > /dev/null 2>&1 || err_code=$?

if [ $err_code = 1 ]; then
  # we need to put the timestamp at the end of the DB name since the naming
  # collision issue
  instance_name="${instance_prefix}-$(date +%s)"
  failover_instance_name="${failover_instance_prefix}-$(date +%s)"

  # create main db
  gcloud beta sql instances create \
    --region $region \
    --database-version $mysql_version \
    --gce-zone $zone \
    --tier $tier \
    --backup --backup-start-time $backup_time \
    --enable-bin-log \
    --storage-size $storage_size \
    --storage-auto-increase \
    $instance_name

  # create failover db, use same code for recreate for db rollback
  gcloud beta sql instances create \
    --region $region \
    --database-version $mysql_version \
    --gce-zone $failover_zone \
    --tier $tier \
    --storage-auto-increase \
    --storage-size $storage_size \
    --master-instance-name $instance_name \
    --replica-type FAILOVER \
    $failover_instance_name

  # root password is optional
  if [[ "$root_password_file" != 'none' ]]; then
    gcloud beta sql users set-password \
      --instance=$instance_name \
      --password $(cat $root_password_file) \
      root %
  fi

  # create a database
  gcloud beta sql databases create \
    --instance=$instance_name \
    $database_name

  # create an application user w/ password
  gcloud beta sql users create \
    --password $(cat $database_password_file) \
    --instance=$instance_name \
    $database_user '%'
elif [ $err_code = 2 ]; then
  die "ERROR: more than one Cloud SQL instance w/ prefix: '$instance_prefix'"
else
  echo "Cloud SQL instance w/ prefix: '$instance_prefix' already created"
fi
