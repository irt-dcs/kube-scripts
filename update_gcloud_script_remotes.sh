#!/bin/bash
# changes origin on repos named "scripts" from:
#   code.stanford.edu/irt-dcs/kube-scripts.git
# to:
#   https://gitlab.med.stanford.edu/irt-public/gcloud-scripts.git
# given $code_directory, your code directory which contains the scripts directories

set -e

code_directory=$1

if [ -z $code_directory ] || ! [ -d $code_directory ]; then
  >&2 echo "usage: $0 [code_directory]"
  exit 1
fi

for script_dir in $(find $code_directory -name scripts); do
  if [ -d $script_dir/.git ]; then
    remote_url=$(git -C $script_dir remote get-url origin)
    if [ $remote_url = https://code.stanford.edu/irt-dcs/kube-scripts.git ]; then
      git -C $script_dir remote set-url origin https://gitlab.med.stanford.edu/irt-public/gcloud-scripts.git
    fi
  fi
done
