#!/bin/bash

###############################################################################
# Authorize all kubernetes nodes to access a cloud sql database
# env arguments:
# - $SQL_INSTANCE_NAME (required)
###############################################################################

# complain if variable is not defined or error
set -ue

# get ips of nodes for the selected cluster
node_ips_spaces=$(kubectl get nodes -o json | \
  jq -r '.items[].status.addresses[]|select(.type=="ExternalIP").address')

node_ips=$(echo $node_ips_spaces | tr ' ' ',')

# add ips of nodes as authorized to access cloud sql
gcloud sql instances patch $SQL_INSTANCE_NAME --authorized-networks $node_ips
